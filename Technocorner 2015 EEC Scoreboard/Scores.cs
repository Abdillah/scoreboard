﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Technocorner_2015_EEC_Scoreboard
{
    class Scores
    {
        /**
         * Control used as scores label holder
         */
        Control board;
        String prefix;
        int count;
        int focused;
        int unfocused;

        int[] scores;

        /**
         * @param board a container which contains score label elements as direct child
         */
        public Scores(Control board, int count)
        {
            this.board = board;
            this.count = count;

            this.scores = new int[count];
            this.prefix = "score_";
            this.focused = 1;
            this.unfocused = 0;
        }

        public String next()
        {
            unfocused = focused;
            focused++;
            if (focused > count)
            {
                focused = 0;
            }

            focus(prefix + focused);

            return prefix + focused;
        }

        public String previous()
        {
            unfocused = focused;
            focused--;
            if (focused < 0)
            {
                focused = count;
            }

            focus(prefix + focused);

            return prefix + focused;
        }

        private void update_focused_score()
        {
            if (focused == 0)
            {
                return;
            }

            // Get id
            String id = prefix + focused;
            Label score = board.Controls[id] as Label;
            int curr_score = scores[focused - 1];
            
            String score_str = curr_score.ToString();

            score.Text = score_str;
        }

        public void increment()
        {
            increment(5);
        }

        public void increment(int delta)
        {
            scores[focused - 1] += delta;
            update_focused_score();
        }

        public void decrement()
        {
            decrement(5);
        }

        public void decrement(int delta)
        {
            scores[focused - 1] -= delta;
            update_focused_score();
        }

        public void reset()
        {
            scores = new int[count];
            
        }

        private void focus(String id)
        {
            Label score;
            Label unfocus;

            // The selector is going out of the view
            if (focused == 0)
            {
                // Remove selector's trace
                unfocus = board.Controls[prefix + unfocused] as Label;
                unfocus.ForeColor = Color.DodgerBlue;
                unfocus.BorderStyle = BorderStyle.None;
                return;
            }

            // Assign focus
            score = board.Controls[id] as Label;
            score.ForeColor = Color.RoyalBlue;
            score.BorderStyle = BorderStyle.FixedSingle;

            // Drop focus to older control
            if (unfocused == 0)
            {
                return;
            }
            unfocus = board.Controls[prefix + unfocused] as Label;            
            unfocus.ForeColor = Color.DodgerBlue;
            unfocus.BorderStyle = BorderStyle.None;
        }
    }
}
