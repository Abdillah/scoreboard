﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Technocorner_2015_EEC_Scoreboard
{
    public partial class ScoreWindow : Form
    {
        string score_prefix;
        const int score_count = 5;
        int score_focused;
        int score_unfocused;

        Scores manager;

        public ScoreWindow()
        {
            InitializeComponent();

            manager = new Scores(this.Controls["score_display_tlayout"], 5);
            score_prefix = "score_";
            score_focused = 1;
            score_unfocused = 5;

            score_focus(score_prefix + score_focused);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private String score_next()
        {
            score_unfocused = score_focused;
            score_focused++;
            if (score_focused > score_count)
            {
                score_focused = 1;
            }

            score_focus(score_prefix + score_focused);

            return score_prefix + score_focused;
        }

        private String score_prev()
        {
            score_unfocused = score_focused;
            score_focused--;
            if (score_focused < 1)
            {
                score_focused = score_count;
            }

            score_focus(score_prefix + score_focused);

            return score_prefix + score_focused;
        }

        private void score_focus(String id)
        {
            // Get table control
            TableLayoutPanel score_display_tlayout = Controls["score_display_tlayout"] as TableLayoutPanel;

            // Assign focus
            Label score = score_display_tlayout.Controls[id] as Label;
            score.ForeColor = Color.RoyalBlue;
            score.BorderStyle = BorderStyle.FixedSingle;

            // Drop focus to older control
            Label unfocus = score_display_tlayout.Controls[score_prefix + score_unfocused] as Label;
            unfocus.ForeColor = Color.DodgerBlue;
            unfocus.BorderStyle = BorderStyle.None;
        }

        private void on_keyup(object sender, KeyEventArgs e)
        {
            // Exit on Escape key
            if (e.KeyCode == Keys.Escape)
            {
                Application.Exit();
            }

            switch (e.KeyCode)
            {
                case Keys.Right:
                    manager.next();
                    break;
                case Keys.Left:
                    manager.previous();
                    break;
                case Keys.Up:
                    manager.increment();
                    break;
                case Keys.Down:
                    manager.decrement();
                    break;
                case Keys.PageUp:
                    manager.increment(10);
                    break;
                case Keys.PageDown:
                    manager.decrement(10);
                    break;
            }
        }
    }
}
