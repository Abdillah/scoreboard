﻿namespace Technocorner_2015_EEC_Scoreboard
{
    partial class ScoreWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.score_display_tlayout = new System.Windows.Forms.TableLayoutPanel();
            this.score_5 = new System.Windows.Forms.Label();
            this.score_4 = new System.Windows.Forms.Label();
            this.score_3 = new System.Windows.Forms.Label();
            this.score_2 = new System.Windows.Forms.Label();
            this.score_1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.score_display_tlayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(1085, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(264, 25);
            this.label10.TabIndex = 19;
            this.label10.Text = "Team E";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(817, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(262, 25);
            this.label9.TabIndex = 18;
            this.label9.Text = "Team D";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(549, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(262, 25);
            this.label8.TabIndex = 17;
            this.label8.Text = "Team C";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(281, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(262, 25);
            this.label7.TabIndex = 16;
            this.label7.Text = "Team B";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(13, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(262, 25);
            this.label6.TabIndex = 15;
            this.label6.Text = "Team A";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // score_display_tlayout
            // 
            this.score_display_tlayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.score_display_tlayout.ColumnCount = 5;
            this.score_display_tlayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.score_display_tlayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.score_display_tlayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.score_display_tlayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.score_display_tlayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.score_display_tlayout.Controls.Add(this.label6, 0, 0);
            this.score_display_tlayout.Controls.Add(this.score_5, 4, 1);
            this.score_display_tlayout.Controls.Add(this.label10, 4, 0);
            this.score_display_tlayout.Controls.Add(this.score_4, 3, 1);
            this.score_display_tlayout.Controls.Add(this.label7, 1, 0);
            this.score_display_tlayout.Controls.Add(this.score_3, 2, 1);
            this.score_display_tlayout.Controls.Add(this.label9, 3, 0);
            this.score_display_tlayout.Controls.Add(this.score_2, 1, 1);
            this.score_display_tlayout.Controls.Add(this.label8, 2, 0);
            this.score_display_tlayout.Controls.Add(this.score_1, 0, 1);
            this.score_display_tlayout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.score_display_tlayout.Location = new System.Drawing.Point(0, 272);
            this.score_display_tlayout.Name = "score_display_tlayout";
            this.score_display_tlayout.Padding = new System.Windows.Forms.Padding(10);
            this.score_display_tlayout.RowCount = 3;
            this.score_display_tlayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.score_display_tlayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.score_display_tlayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.score_display_tlayout.Size = new System.Drawing.Size(1362, 335);
            this.score_display_tlayout.TabIndex = 20;
            // 
            // score_5
            // 
            this.score_5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.score_5.AutoSize = true;
            this.score_5.Font = new System.Drawing.Font("Impact", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score_5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.score_5.Location = new System.Drawing.Point(1085, 57);
            this.score_5.Name = "score_5";
            this.score_5.Size = new System.Drawing.Size(264, 117);
            this.score_5.TabIndex = 14;
            this.score_5.Text = "0";
            this.score_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // score_4
            // 
            this.score_4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.score_4.AutoSize = true;
            this.score_4.Font = new System.Drawing.Font("Impact", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score_4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.score_4.Location = new System.Drawing.Point(817, 57);
            this.score_4.Name = "score_4";
            this.score_4.Size = new System.Drawing.Size(262, 117);
            this.score_4.TabIndex = 13;
            this.score_4.Text = "0";
            this.score_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // score_3
            // 
            this.score_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.score_3.AutoSize = true;
            this.score_3.Font = new System.Drawing.Font("Impact", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score_3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.score_3.Location = new System.Drawing.Point(549, 57);
            this.score_3.Name = "score_3";
            this.score_3.Size = new System.Drawing.Size(262, 117);
            this.score_3.TabIndex = 12;
            this.score_3.Text = "0";
            this.score_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // score_2
            // 
            this.score_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.score_2.AutoSize = true;
            this.score_2.Font = new System.Drawing.Font("Impact", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score_2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.score_2.Location = new System.Drawing.Point(281, 57);
            this.score_2.Name = "score_2";
            this.score_2.Size = new System.Drawing.Size(262, 117);
            this.score_2.TabIndex = 11;
            this.score_2.Text = "0";
            this.score_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // score_1
            // 
            this.score_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.score_1.AutoSize = true;
            this.score_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.score_1.Font = new System.Drawing.Font("Impact", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score_1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.score_1.Location = new System.Drawing.Point(13, 57);
            this.score_1.Name = "score_1";
            this.score_1.Size = new System.Drawing.Size(262, 119);
            this.score_1.TabIndex = 10;
            this.score_1.Text = "0";
            this.score_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackgroundImage = global::Technocorner_2015_EEC_Scoreboard.Properties.Resources.logo_header;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1338, 224);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // ScoreWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1362, 607);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.score_display_tlayout);
            this.Name = "ScoreWindow";
            this.Text = "Technocorner 2015 EEC Scoreboard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.on_keyup);
            this.score_display_tlayout.ResumeLayout(false);
            this.score_display_tlayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel score_display_tlayout;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label score_5;
        private System.Windows.Forms.Label score_4;
        private System.Windows.Forms.Label score_3;
        private System.Windows.Forms.Label score_2;
        private System.Windows.Forms.Label score_1;




    }
}

